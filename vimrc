syntax enable

set noerrorbells
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set nu
set nowrap
set incsearch

call plug#begin('~/.vim/plugged')

Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'vim-airline/vim-airline'
Plug 'valloric/youcompleteme'
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'
Plug 'flazz/vim-colorschemes'

call plug#end()

:colorscheme molokai

" Disable markdown folding (enabled by default)
let g:vim_markdown_folding_disabled = 1

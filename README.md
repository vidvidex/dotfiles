# Dotfiles

Dotfiles are managed using [dotbot](https://github.com/anishathalye/dotbot)

## Install

- clone this repository
- install packages
    ```sh
    ./pkg-install.sh
    ```
- unlock secret files
    ```sh
    git-crypt unlock
    ```
- run dotbot
    ```sh
    ./install
    ```